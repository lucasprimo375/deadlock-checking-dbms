import java.util.*;

public class TrManager{
	public int numSequencia;
	public int time;
	public LinkedList<Tr> transacoes = new LinkedList<Tr>();
	public LinkedList<Item> itensDados = new LinkedList<Item>();
	
	public TrManager(){
		this.numSequencia = 0;
		this.time = 0;
	}
	
	public Item criarItem( int id ){
		Item I = new Item( id );
		this.itensDados.add( I );
		
		return I;
	}
	
	public Tr criarTr(){
		Tr T = new Tr(this.numSequencia, this.time, 0); // 0 - estado ativo
		this.numSequencia++;
		this.time+=5;
		this.transacoes.add( T );
		
		return T;
	}
	
	public void mostrarEstados(){
		System.out.println("Estados das transações");
		String estado = "";
		
		for( Tr T : transacoes ){
			if( T.state == 0 ){
				estado = "ativa";
			} else {
				if( T.state == 1 ){
					estado = "esperando";
				} else {
					if( T.state == 2 ){
						estado = "commitada";
					} else { // T.state é 3, isto é, T foi abortada
						estado = "abortada";
					}
				}
			}
			
			System.out.println("Transação " + T.id + ": " + estado);
		}
		
		System.out.println();
	}
}
