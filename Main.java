public class Main{
	public static void main( String[] args ){
		TrManager TrM = new TrManager();
		LockManager LM = new LockManager();
		
		Item I0 = TrM.criarItem(0);
		Item I1 = TrM.criarItem(1);
		Item I2 = TrM.criarItem(2);
		
		System.out.println( "Rodando o exemplo do enunciado\n" );
		
		Tr tr0 = TrM.criarTr();
		System.out.println( "Transação 0 iniciada" );
		TrM.mostrarEstados();
		
		tr0.ler( I0, LM, TrM );
		System.out.println( "Transação 0 lê item de dados 0" );
		TrM.mostrarEstados();
		
		Tr tr1 = TrM.criarTr();
		System.out.println( "Transação 1 iniciada" );
		TrM.mostrarEstados();
		
		System.out.println( "Transação 1 escreve no item de dados 0" );
		tr1.escrever( I0, LM, TrM );
		TrM.mostrarEstados();
		
		System.out.println( "Transação 0 lê item de dados 1" );
		tr0.ler( I1, LM, TrM );
		TrM.mostrarEstados();
	
		System.out.println( "Transação 0 vai dar commit" );
		tr0.commit( LM, TrM );
		TrM.mostrarEstados();
		
		System.out.println( "Transação 1 lê item de dados 2" );
		tr1.ler( I2, LM, TrM );
		TrM.mostrarEstados();
		
		System.out.println( "Transação 1 vai dar commit" );
		tr1.commit( LM, TrM );
		TrM.mostrarEstados();
		
		System.out.println( "Retomando a transação 0\n" );
		
		System.out.println( "Transação 0 lê item de dados 1" );
		tr0.ler( I1, LM, TrM );
		TrM.mostrarEstados();
	
		System.out.println( "Transação 0 vai dar commit" );
		tr0.commit( LM, TrM );
		TrM.mostrarEstados();
		
		System.out.println( "Plano finalizado" );
	}
}
