public class LockManager{
	// tabela de bloqueios: lockTable - linha i: id do item i, coluna j: id da transação j, posição [i][j]: tipo de bloqueio(0-não tem bloqueio,1-compartilhado,2-exclusivo)
	public int[][] lockTable = new int[3][2];
	
	public LockManager(){
		for( int i = 0; i<3; i++ ){
			for( int j = 0; j<2; j++ ){
				lockTable[i][j] = 0;
			}
		}
	}
	
	// lista de espera: para cada item de dado, uma lista de transações que esperam pelo item e o modo de bloqueio
	// hashmap chave é o item e chave (tr, modo)
	// implementada na classe do item
	
	public boolean LS( Tr T, Item I, TrManager TrM ){
		for( int i=0; i<2; i++ ){
			if( lockTable[I.itemId][i] == 2 ){
				Tr TrAtual = TrM.transacoes.get(i);
				
				if( TrAtual.timestamp > T.timestamp ){
					T.state = 1;
					I.waitList.add( new Bloqueio(T,"S") );
					return false;
				} else {
					T.state = 3;
					return false;
				}	
			}
		}
		
		lockTable[I.itemId][T.id] = 1;
		
		return true;
	}
	
	public boolean LX( Tr T, Item I, TrManager TrM ){
		for( int i=0; i<2; i++ ){
			if( lockTable[I.itemId][i] == 2 ){
				Tr TrAtual = TrM.transacoes.get(i);
				
				if( TrAtual.timestamp > T.timestamp ){
					T.state = 1;
					I.waitList.add( new Bloqueio(T,"X") );
					return false;
				} else {
					T.state = 3;
					return false;
				}	
			}
		}
		
		for( int j=0; j<2; j++ ){
			 if( lockTable[I.itemId][j] != 0 ){
			 	TrM.transacoes.get(j).state = 1;
			 	I.waitList.add( new Bloqueio(TrM.transacoes.get(j), "S") );
			 }
		}
		
		lockTable[I.itemId][T.id] = 2;
		
		return true;
	}
	
	public void U( Tr T, Item I, TrManager TrM ){
		for( int i=0; i<2; i++ ){
			lockTable[I.itemId][i] = 0;
		}
		
		//T.state = 2;
		
		Bloqueio B = I.waitList.poll();
		
		if( B != null ){
			Tr proxTr = B.T;
			String modo = B.modo;
			proxTr.state = 0;
			
			if( modo == "S" ){
				this.LS( proxTr, I, TrM );
			} else { // modo é X
				this.LX( proxTr, I, TrM );
			}
		}
	}
}
