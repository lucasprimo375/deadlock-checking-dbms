public class Tr{
	public int id;
	public int timestamp;
	public int state; // 0 - ativa, 1 - esperando, 2 - commitada, 3 - abortada
	
	public Tr( int id, int timestamp, int state ){
		this.id = id;
		this.timestamp = timestamp;
		this.state = state;
	}
	
	// implementar métodos de ler e escrever?
	public void ler( Item I, LockManager LM, TrManager TrM ){
		if( this.state == 0 ){
			LM.LS( this, I, TrM );
		}
	}
	
	public void escrever( Item I, LockManager LM, TrManager TrM ){
		if( this.state == 0 ){
			LM.LX( this, I, TrM );
		}
	}
	
	public void commit( LockManager LM, TrManager TrM ){
		if( this.state == 0 ){
			for( int i=0; i<3; i++ ){
				Item I = TrM.itensDados.get( i );
				LM.U( this, I, TrM );
				this.state = 2;
			}
		}
	}
}
